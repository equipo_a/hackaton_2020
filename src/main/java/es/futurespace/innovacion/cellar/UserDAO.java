package es.futurespace.innovacion.cellar;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class UserDAO {

    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        Connection c = null;
    	String sql = "SELECT * FROM user ORDER BY name";
        try {
            c = ConnectionHelper.getConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }

    
    public List<User> findByName(String name, String password) {
        List<User> list = new ArrayList<>();
        Connection c = null;
    	String sql = "SELECT * FROM user as e " +
			"WHERE name = '"+name +
			"' and password = '"+password+"'";
        try {
        	c = ConnectionHelper.getConnection();
            Statement sentencia = c.createStatement();           
            ResultSet rs = sentencia.executeQuery(sql);
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }
    
    public User findById(int id) {
    	String sql = "SELECT * FROM user WHERE id = '"+id+"'";
        User user = null;
        Connection c = null;
        try {
        	c = ConnectionHelper.getConnection();
            Statement sentencia = c.createStatement();           
            ResultSet rs = sentencia.executeQuery(sql);
            if (rs.next()) {
                user = processRow(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return user;
    }

    public User save(User user)
	{
	    return user.getId() > 0 ? update(user) : create(user);
	}    
    
    public User create(User user) {
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO user (name, password, role, cuenta) VALUES ('"+user.getName()+"','"+ user.getPassword()+"','"+ user.getRole()+"','"+ user.getCuenta()+"')";
        try {
            c = ConnectionHelper.getConnection();
            PreparedStatement sentencia = c.prepareStatement(sql, new String[]{"ID"});             
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int id = rs.getInt(1);
            user.setId(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return user;
    }

    public User update(User user) {
        Connection c = null;
        String sql = "UPDATE user SET name='"+user.getName()+"',"+ "password='"+user.getPassword()+"',"+" role='"+user.getRole()+"',"+ "cuenta='"+user.getCuenta()+"' WHERE id='"+user.getId()+"'";
        try {
        	c = ConnectionHelper.getConnection();
            Statement sentencia = c.createStatement();           
            sentencia.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return user;
    }

    public boolean remove(int id) {
        Connection c = null;
        String sql = "DELETE FROM user WHERE id='"+id+"'";
        try {
        	 c = ConnectionHelper.getConnection();
             Statement sentencia = c.createStatement();
            int count = sentencia.executeUpdate(sql);
            return count == 1;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
    }

    protected User processRow(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setName(rs.getString("name"));
        user.setPassword(rs.getString("password"));
        user.setRole(rs.getString("role"));
        user.setCuenta(rs.getString("cuenta"));
        return user;
    }
    
}
