package es.futurespace.innovacion.cellar;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christophe Coenraets
 */
public class WineDAO {

    public List<Wine> findAll() {
        List<Wine> list = new ArrayList<Wine>();
        Connection c = null;
    	String sql = "SELECT * FROM wine ORDER BY name";
        try {
            c = ConnectionHelper.getConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }

    
    public List<Wine> findByName(String name) {
        List<Wine> list = new ArrayList<Wine>();
        Connection c = null;
    	String sql = "SELECT * FROM wine as e " +
			"WHERE UPPER(name) LIKE '%"+name+ "%'";
        try {
        	c = ConnectionHelper.getConnection();
            Statement sentencia = c.createStatement();           
            ResultSet rs = sentencia.executeQuery(sql);
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return list;
    }
    
    public Wine findById(int id) {
    	String sql = "SELECT * FROM wine WHERE id = '"+ id+ "'";
        Wine wine = null;
        Connection c = null;
        try {
            c = ConnectionHelper.getConnection();
            Statement sentencia = c.createStatement();           
            ResultSet rs = sentencia.executeQuery(sql);
            if (rs.next()) {
                wine = processRow(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return wine;
    }

    public Wine save(Wine wine)
	{
		return wine.getId() > 0 ? update(wine) : create(wine);
	}    
    
    public Wine create(Wine wine) {
        Connection c = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO wine (name, grapes, country, region, year, picture, description) VALUES ('"+wine.getName()+"','"+wine.getGrapes()+"','"+ wine.getCountry()+"','"+ wine.getRegion()+"','"+ wine.getYear()+"','"+ wine.getPicture()+"','"+wine.getDescription()+"')";
        try {
        	 c = ConnectionHelper.getConnection();
        	 PreparedStatement sentencia = c.prepareStatement(sql, new String[]{"ID"});
        	 sentencia.executeUpdate();
             ResultSet rs = sentencia.getGeneratedKeys();
             rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int id = rs.getInt(1);
            wine.setId(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return wine;
    }

    public Wine update(Wine wine) {
        Connection c = null;
        String sql = "UPDATE wine SET name='"+wine.getName()+"',"+ "grapes='"+wine.getGrapes()+"',"+ "country='"+wine.getCountry()+"',"+ "region='"+wine.getRegion()+"',"+ "year='"+wine.getYear()+"'," +"picture='"+wine.getPicture()+"'," +"description='"+wine.getDescription()+"' WHERE id='"+wine.getId()+"'";
        try {
        	c = ConnectionHelper.getConnection();
            Statement sentencia = c.createStatement();           
            sentencia.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
        return wine;
    }

    public boolean remove(int id) {
        Connection c = null;
        String sql = "DELETE FROM wine WHERE id='"+id+"'";
        try {
        	c = ConnectionHelper.getConnection();
            Statement sentencia = c.createStatement();           
            int count = sentencia.executeUpdate(sql);
            return count == 1;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		} finally {
			ConnectionHelper.close(c);
		}
    }

    protected Wine processRow(ResultSet rs) throws SQLException {
        Wine wine = new Wine();
        wine.setId(rs.getInt("id"));
        wine.setName(rs.getString("name"));
        wine.setGrapes(rs.getString("grapes"));
        wine.setCountry(rs.getString("country"));
        wine.setRegion(rs.getString("region"));
        wine.setYear(rs.getString("year"));
        wine.setPicture(rs.getString("picture"));
        wine.setDescription(rs.getString("description"));
        return wine;
    }
    
}
