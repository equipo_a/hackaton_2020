package es.futurespace.innovacion.cellar;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.List;
@Path("/login")
public class LoginService {
    private UserDAO dao = new UserDAO();

    @GET
    @Path("login")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public User login(@QueryParam("username") String username, @QueryParam("password") String password) {
        System.out.println("Login: " + username);
        List<User> u = dao.findByName(username,password);
        if (u == null || u.isEmpty()) {
            System.out.println("User is null");
            return null;
        }else {
        	return u.get(0);
        }
    }
}