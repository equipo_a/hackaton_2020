// The root URL for the RESTful services
var rootURL = "http://hackaton-team2.futurespace.corp:8080/cellar/rest/login";

var currentUser;

$('#btnLogin').click(function() {
	login($('#user').val(), $('#password').val());
	return false;
});

$('#btnCreate').click(function() {
	window.location = "create.html";
	return false;
});


function login(username, password) {
	console.log('Login user: ' + username + '/' + password);
	$.ajax({
		type: 'GET',
		url: rootURL + '/login?username=' + username + '&password=' + password,
		dataType: "json",
		success: accessAllowed,
		error: accessDenied
	});
}

function accessAllowed(data){
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	if (data == null){
	    accessDenied(data)
	    return false;
    }
	console.log('Logged successfully');
	currentUser = data
	sessionStorage.setItem("Role",data.role);
	window.location = "cellar.html";
}

function accessDenied(data){
	console.log('Logging failed');
	alert('Usuario o password incorrectos');
}
